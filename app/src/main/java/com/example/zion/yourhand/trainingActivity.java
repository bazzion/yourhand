package com.example.zion.yourhand;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class trainingActivity extends AppCompatActivity implements SensorEventListener {

  Button mButtonStart;
  Button mButtonStop;
  Button mButtonStats;
  CheckBox timeVisibleCheckBox;
  EditText timeTextEditor;
  EditText countRotations;
  TextView tCount;
  TextView numberOfRotation;
  TextView secTime;
  TextView timeLeft;

  Intent intent;

  Timer timer;
  TimerTask mTimerTask;

  public int cRotation;
  public int mTime;
  public int angleR = 0;
  public int myTime;
  public Date startTime;
  public Date cancelTime;

  boolean firstCheck = false;
  boolean secondCheck = false;


  private static final int ANGLE_180_DEGREE = 180;
  private static final int ANGLE_120_DEGREE = 130;
  private static final int ANGLE_90_DEGREE = 90;
  private static final int ANGLE_MINUS_180_DEGREE = -180;
  private static final int ANGLE_MINUS_120_DEGREE = -130;
  private static final int ANGLE_MINUS_90_DEGREE = -90;

  private static final int ANGLE_140_DEGREE = 140;
  private static final int ANGLE_MINUS_140_DEGREE = -140;

  private static final double RADIAN_ANGLE = 57.2957795;

  private static final int EXTENT_TWO = 2;


  private SensorManager sm;

  DB db;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_training);

    mButtonStart = findViewById(R.id.start_button);
    mButtonStop = findViewById(R.id.stop_button);
    mButtonStats = findViewById(R.id.stats_button);
    timeVisibleCheckBox = findViewById(R.id.time_checkBox);
    timeTextEditor = findViewById(R.id.time_editor);
    countRotations = findViewById(R.id.input_rotate_count);
    tCount = findViewById(R.id.t_count_rotation);
    numberOfRotation = findViewById(R.id.t_number_of_rotation);

    secTime = findViewById(R.id.time_sec);
    timeLeft = findViewById(R.id.time_left);

    intent = new Intent(getApplicationContext(), statsActivity.class);

    db = new DB(this);
    db.open();

    sm = (SensorManager) getSystemService(SENSOR_SERVICE);
    if (sm != null && sm.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0) {
      Sensor s = sm.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
      sm.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
    }

    timeVisibleCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.time_checkBox) {
          if (isChecked) {
            timeTextEditor.setVisibility(View.VISIBLE);
          } else {
            timeTextEditor.setVisibility(View.INVISIBLE);
          }
        }
      }
    });

    mButtonStats.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        startActivity(intent);
      }
    });

    mButtonStart.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        onResume();
        startButtonVisibility();

      }

    });

    mButtonStop.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(View v) {
        stopButtonVisibility();
      }
    });
  }


  public void startButtonVisibility() {
    try {
      if (timeVisibleCheckBox.isChecked()) {
        mTime = Integer.parseInt(timeTextEditor.getText().toString());
      }
      cRotation = Integer.parseInt(countRotations.getText().toString());
      numberOfRotation.setText(String.valueOf(cRotation));
    } catch (NumberFormatException nfe) {
      System.out.println("Could not parse" + nfe);
    }

    myTime = mTime;
    myTime = myTime * 60;

    if (timeVisibleCheckBox.isChecked() && mTime != 0) {
      timeLeft.setVisibility(View.VISIBLE);
      secTime.setVisibility(View.VISIBLE);
      myTimer();
    }
    if (cRotation != 0) {
      startTime = new Date();
      angleR = 0;
      mButtonStart.setVisibility(View.INVISIBLE);
      timeVisibleCheckBox.setVisibility(View.INVISIBLE);
      timeTextEditor.setVisibility(View.INVISIBLE);
      countRotations.setVisibility(View.INVISIBLE);
      mButtonStats.setVisibility(View.INVISIBLE);
      mButtonStop.setVisibility(View.VISIBLE);
      tCount.setVisibility(View.VISIBLE);
      numberOfRotation.setVisibility(View.VISIBLE);
    }
  }

  public void myTimer() {
    if (timer != null) {
      timer.cancel();
    }

    timer = new Timer();
    mTimerTask = new myTimerTask();

    timer.schedule(mTimerTask, 0, 1000);
  }

  class myTimerTask extends TimerTask {

    @Override
    public void run() {
      if (myTime > 0) {
        myTime--;
        final String leftTime = Integer.toString(myTime);
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            secTime.setText(leftTime);
            if (myTime == 0) {
              timer.cancel();
              timer = null;
              stopButtonVisibility();
            }
          }
        });
      }
    }
  }

  public void stopButtonVisibility() {
    timeLeft.setVisibility(View.INVISIBLE);
    secTime.setVisibility(View.INVISIBLE);
    mButtonStop.setVisibility(View.INVISIBLE);
    tCount.setVisibility(View.INVISIBLE);
    numberOfRotation.setVisibility(View.INVISIBLE);
    mButtonStats.setVisibility(View.VISIBLE);
    mButtonStart.setVisibility(View.VISIBLE);
    timeVisibleCheckBox.setVisibility(View.VISIBLE);
    countRotations.setVisibility(View.VISIBLE);

    cancelTime = new Date();
    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(
        "yyyy.MM.dd ' ' hh.mm.ss a");

    Cursor cursor = db.getAllData();
    long cnt = cursor.getCount();
    cursor.close();
    if (cnt < 50) {
      db.addRec(angleR, String.valueOf(format.format(startTime)),
          String.valueOf(format.format(cancelTime)));
    } else {
      Toast.makeText(this, R.string.stats_toast, Toast.LENGTH_SHORT).show();
    }

    if (timeVisibleCheckBox.isChecked() && mTime != 0) {
      if (timer != null) {
        timer.cancel();
        timer = null;
      }
    }

    onPause();

    if (cRotation == angleR && cRotation != 0) {
      Toast.makeText(this, R.string.win_toast, Toast.LENGTH_SHORT).show();
    }
  }


  @Override
  public void onSensorChanged(SensorEvent event) {

    if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
      return;
    }

    float x = event.values[0];
    float y = event.values[1];
    float z = event.values[2];

    double angle = (Math.atan((x / Math.sqrt(Math.pow(y, EXTENT_TWO) + Math.pow(z, EXTENT_TWO))))
        * RADIAN_ANGLE);

    if (y < 0) {
      if (angle >= 0) {
        angle = ANGLE_180_DEGREE - angle;
      } else {
        angle = ANGLE_MINUS_180_DEGREE - angle;
      }
    }

    if (angle >= ANGLE_90_DEGREE && angle <= ANGLE_120_DEGREE) {
      firstCheck = true;
    }
    if (angle <= ANGLE_MINUS_90_DEGREE && angle >= ANGLE_MINUS_120_DEGREE) {
      secondCheck = true;
    }

    if (angle >= ANGLE_140_DEGREE && angle <= ANGLE_MINUS_180_DEGREE) {
      firstCheck = false;
      secondCheck = false;
    }
    if (angle <= ANGLE_MINUS_140_DEGREE && angle >= ANGLE_MINUS_180_DEGREE) {
      secondCheck = false;
      firstCheck = false;

    }

    if (firstCheck && secondCheck) {
      angleR++;
      firstCheck = false;
      secondCheck = false;
    }
    tCount.setText(String.valueOf(angleR));

    if (cRotation == angleR && cRotation != 0) {
      stopButtonVisibility();
      Toast.makeText(this, R.string.win_toast, Toast.LENGTH_SHORT).show();
      cRotation = 0;
    }
  }

  @Override
  public void onAccuracyChanged(Sensor sensor, int accuracy) {
  }

  @Override
  protected void onResume() {
    super.onResume();
    db.open();
    if (sm.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0) {
      Sensor s = sm.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
      sm.registerListener(this, s, SensorManager.SENSOR_DELAY_NORMAL);
    }
  }

  @Override
  protected void onPause() {
    sm.unregisterListener(this);
    super.onPause();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    db.close();
  }

}

