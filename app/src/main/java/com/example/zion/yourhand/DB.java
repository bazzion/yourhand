package com.example.zion.yourhand;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DB {

  public static final String DB_NAME = "stats";
  private static final int DB_VERSION = 1;
  private static final String DB_TABLE = "mytab";

  public static final String COLUMN_NAME_STATS_ID = "_id";
  public static final String COLUMN_NAME_COUNT_ROTATION = "countRotation";
  public static final String COLUMN_NAME_START_TIME = "startTime";
  public static final String COLUMN_NAME_CANCEL_TIME = "cancelTime";
  public static final String COLUMN_NAME_COMMENT = "yourComment";

  private static final String DB_CREATE =
      "create table " + DB_TABLE + "(" +
          COLUMN_NAME_STATS_ID + " integer primary key autoincrement, " +
          COLUMN_NAME_COUNT_ROTATION + " integer, " +
          COLUMN_NAME_START_TIME + " text, " +
          COLUMN_NAME_CANCEL_TIME + " text," +
          COLUMN_NAME_COMMENT + " text" +
          ");";

  private final Context mCtx;


  private DBHelper mDBHelper;
  private SQLiteDatabase mDB;

  public DB(Context ctx) {
    mCtx = ctx;
  }

  // open connection
  public void open() {
    mDBHelper = new DBHelper(mCtx, DB_NAME, null, DB_VERSION);
    mDB = mDBHelper.getWritableDatabase();
  }

  // close connection
  public void close() {
    if (mDBHelper != null) {
      mDBHelper.close();
    }
  }

  // get all data
  public Cursor getAllData() {
    return mDB.query(DB_TABLE, null, null, null, null, null, null);
  }

  public void addComment(String statsComment, long id) {
    ContentValues cv = new ContentValues();
    cv.put(COLUMN_NAME_COMMENT, statsComment);
    mDB.update(DB_TABLE, cv, "_id=" + id, null);
  }

  public Cursor getComment(long id) {
    return mDB.query(DB_TABLE, new String[]{COLUMN_NAME_COMMENT}, "_id = ?",
        new String[]{Long.toString(id)}, null, null, null);

  }

  // recording note
  public void addRec(int statsRotation, String statsStart, String statsCancel) {
    ContentValues cv = new ContentValues();
    cv.put(COLUMN_NAME_COUNT_ROTATION, statsRotation);
    cv.put(COLUMN_NAME_START_TIME, statsStart);
    cv.put(COLUMN_NAME_CANCEL_TIME, statsCancel);
    mDB.insert(DB_TABLE, null, cv);
  }

  //deleting note
  public void delRec(long id) {
    mDB.delete(DB_TABLE, COLUMN_NAME_STATS_ID + "=" + id, null);
  }


  private class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context, String name, CursorFactory factory, int version) {
      super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
      db.execSQL(DB_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
      db.execSQL("DROP TABLE IF EXISTS " + DB_TABLE);
      onCreate(db);
    }
  }
}
