package com.example.zion.yourhand;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.InputFilter;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.ListView;

public class statsActivity extends FragmentActivity implements LoaderCallbacks<Cursor> {


  ListView lvData;
  DB db;
  SimpleCursorAdapter scAdapter;

  private static final int DELETE_ID = 1;
  private static final int COMMENT_ID = 2;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_stats);

    db = new DB(this);
    db.open();

    String[] from = new String[]{DB.COLUMN_NAME_COUNT_ROTATION, DB.COLUMN_NAME_START_TIME,
        DB.COLUMN_NAME_CANCEL_TIME, DB.COLUMN_NAME_COMMENT};
    int[] to = new int[]{R.id.stats_count_rotation, R.id.stats_start_time, R.id.stats_cancel_time,
        R.id.stats_comment};

    scAdapter = new SimpleCursorAdapter(this, R.layout.item, null, from, to, 0);
    lvData = findViewById(R.id.lvData);
    lvData.setAdapter(scAdapter);

    registerForContextMenu(lvData);

    getSupportLoaderManager().initLoader(0, null, this);

  }

  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);
    menu.add(0, DELETE_ID, 0, R.string.delete_record);
    menu.add(0, COMMENT_ID, 0, R.string.add_comment);
  }

  public boolean onContextItemSelected(MenuItem item) {
    if (item.getItemId() == DELETE_ID) {
      AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) item.getMenuInfo();
      db.delRec(acmi.id);
      getSupportLoaderManager().getLoader(0).forceLoad();
    }

    if (item.getItemId() == COMMENT_ID) {
      final AdapterContextMenuInfo acmi = (AdapterContextMenuInfo) item.getMenuInfo();

      AlertDialog.Builder alert = new Builder(this);

      alert.setTitle(R.string.alert_title);
      alert.setMessage(R.string.alert_message);

      final EditText commentText = new EditText(this);
      int maxLength = 250;
      InputFilter[] filters = new InputFilter[1];
      filters[0] = new InputFilter.LengthFilter(maxLength);
      commentText.setFilters(filters);
      alert.setView(commentText);

      Cursor cursor = db.getComment(acmi.id);
      if (cursor != null) {
        if (cursor.moveToFirst()) {
          String curComment = cursor.getString(cursor.getColumnIndex("yourComment"));
          commentText.setText(curComment);
        }
      }

      alert.setPositiveButton(R.string.alert_positive_button, new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
          String value = commentText.getText().toString();
          db.addComment(value, acmi.id);
          getSupportLoaderManager().getLoader(0).forceLoad();
        }
      });

      alert.setNegativeButton(R.string.alert_cancel_button, new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int whichButton) {
          dialog.cancel();
        }
      });

      alert.show();
    }
    return super.onContextItemSelected(item);
  }

  @Override
  protected void onResume() {

    db.open();

    String[] from = new String[]{DB.COLUMN_NAME_COUNT_ROTATION, DB.COLUMN_NAME_START_TIME,
        DB.COLUMN_NAME_CANCEL_TIME, DB.COLUMN_NAME_COMMENT};
    int[] to = new int[]{R.id.stats_count_rotation, R.id.stats_start_time, R.id.stats_cancel_time,
        R.id.stats_comment};

    getSupportLoaderManager().getLoader(0).forceLoad();

    scAdapter = new SimpleCursorAdapter(this, R.layout.item, null, from, to, 0);
    lvData = findViewById(R.id.lvData);
    lvData.setAdapter(scAdapter);

    super.onResume();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    db.close();
  }

  @Override
  public Loader<Cursor> onCreateLoader(int id, Bundle args) {
    return new MyCursorLoader(this, db);
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    scAdapter.swapCursor(data);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader) {

  }


  static class MyCursorLoader extends CursorLoader {

    DB db;

    public MyCursorLoader(Context context, DB db) {
      super(context);
      this.db = db;
    }

    @Override
    public Cursor loadInBackground() {
      Cursor cursor = db.getAllData();
      return cursor;
    }
  }
}


