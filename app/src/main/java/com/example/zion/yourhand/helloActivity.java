package com.example.zion.yourhand;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import java.util.Timer;
import java.util.TimerTask;

public class helloActivity extends AppCompatActivity {

  private static int TIME_OUT = 5000;
  Timer timer = new Timer();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_hello);

    timer.scheduleAtFixedRate(new TimerTask() {
      @Override
      public void run() {
        runOnUiThread(new Runnable() {
          @Override
          public void run() {
            timer.cancel();
            Intent intent = new Intent(helloActivity.this, trainingActivity.class);
            startActivity(intent);
            finish();
          }
        });
      }
    }, TIME_OUT, TIME_OUT);
  }
}
